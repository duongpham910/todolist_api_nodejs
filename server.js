var express = require('express');
var bodyParser = require('body-parser');
var _ = require('underscore');
var db = require('./db.js');

var app = express();
var PORT = process.env.PORT || 3000;
var todoNextId = 4;
var todos = [{
  id: 1,
  description: 'Build a simple API - nodejs',
  completed: false
}, {
  id: 2,
  description: 'Go to T-beer - team building',
  completed: false
}, {
  id: 3,
  description: 'Feed the dog ',
  completed: true
}];

app.use(bodyParser.json())

app.get('/', function(req, res) {
  res.send('To do API Root')
});

// GET /todos
app.get('/todos', function(req, res) {
  var query = req.query;
  var where = {};

  if (query.hasOwnProperty('completed') && query.completed == 'true') {
    where.completed = true;
  } else if (query.hasOwnProperty('completed') && query.completed == 'false') {
    where.completed = false;
  }

  if (query.hasOwnProperty('search') && query.search.length > 0) {
    where.description = {
      $like: '%' + query.search + '%'
    };
  }

  db.todo.findAll({where: where}).then(function (todos) {
    res.json(todos);
  }).catch(function (e) {
    res.status(500).json(e);
  });
});

// GET /todos/:id
app.get('/todos/:id', function(req, res) {
  var todoId = parseInt(req.params.id, 10);
  db.todo.findById(todoId).then(function (todo) {
    if (todo) {
      res.json(todo.toJSON());
    } else {
      res.status(404).send();
    }
  }).catch(function (e) {
    res.status(500).json(e);
  });
});

// POST /todos
app.post('/todos', function(req, res) {
  var body = _.pick(req.body, 'description', 'completed');

  db.todo.create(body).then(function (todo) {
    res.json(todo.toJSON());
  }).catch(function (e) {
    res.status(400).json(e);
  });

});

// DELETE /todos/:id
app.delete('/todos/:id', function(req, res) {
  var todoId = parseInt(req.params.id, 10);

  db.todo.destroy({where: {id: todoId}}).then(function(rowsDeleted) {
    if (rowsDeleted == 0) {
      res.status(404).json({
        "error": "no todo found with that id"
      });
    } else {
      res.status(204).send();
    }
  }).catch(function(e){
    res.status(500).json(e);
  });
});

// PUT /todos/:id
app.put('/todos/:id', function(req, res) {
  var body = _.pick(req.body, 'description', 'completed');
  var attributes = {}

  var todoId = parseInt(req.params.id, 10);

  if (body.hasOwnProperty('completed')) {
    attributes.completed = body.completed;
  }

  if (body.hasOwnProperty('description')) {
    attributes.description = body.description;
  }

  db.todo.findById(todoId).then(function(todo) {
    if (todo) {
      todo.update(attributes).then(function(todo){
        res.json(todo.toJSON());
      }, function(e) {
        res.status(400).json(e);
      });
    } else {
      res.status(404).send();
    }
  }, function(e) {
    res.status(500).send();
  });

});

db.sequelize.sync().then(function () {
  app.listen(PORT, function() {
    console.log('Express listening on port' + PORT + '!');
  });
});
